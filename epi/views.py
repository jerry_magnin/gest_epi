from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from .models import Epi, Kit

# Create your views here.
def index(request):
    latest_epi_list = Epi.objects.order_by('-id')[:5]
    context = {'latest_epi_list': latest_epi_list}
    return render(request, 'epi/index.html', context)

def detail_epi(request, epi_id):
    epi = Epi.objects.get(pk=epi_id)
    epiname = "{} {}".format(epi.e_brand, epi.e_model)
    inspections = epi.inspection_set.all()
    context = {'epi': epi, 'epiname': epiname, 'inspections': inspections}
    return render(request, 'epi/epi_detail.html', context)

def kits(request):
    kits = Kit.objects.order_by('-k_name')
    context = {'kits': kits}
    return render(request, 'epi/kits.html', context)

def detail_kit(request, kit_id):
    kit = Kit.objects.get(pk=kit_id)
    kitname = kit.k_name
    
    validEPIList = []
    for epi in kit.epi_set.all():
        inspection = epi.inspection_set.last()
        if inspection is not None:
            if inspection.e_result != 'RE':
                validEPIList.append(epi)
        else:
            validEPIList.append(epi)
    
    context = {'validEPIList':validEPIList, 'kitname':kitname}
    return render(request, 'epi/kit_detail.html', context)
