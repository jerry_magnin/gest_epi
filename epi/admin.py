from django.contrib import admin
from django.utils.html import format_html
from .models import Type, Kit, Epi, Brand, Inspection

# Register your models here.
admin.site.register(Type)
admin.site.register(Brand)
admin.site.register(Inspection)

class InspectionInline(admin.StackedInline):
    model = Inspection
    extra = 1

class EpiAdmin(admin.ModelAdmin):
    fieldsets = [
            ('Informations génériques', {'fields': ['e_brand',
                                                    'e_model',
                                                    'e_type']}),
            ('Détails', {'fields': ['e_serial',
                                    'e_kit',
                                    'e_buyDate']})
            ]
    inlines = [InspectionInline]
    
    def lastInspectionResult(self, obj):
        if obj.inspection_set.last() is not None:
            return obj.inspection_set.last().e_result
    lastInspectionResult.short_description = "Résultat de dernière inspection"
    
    list_display = ['e_type', 'e_brand', 'e_model', 'e_kit', 'e_buyDate', 'lastInspectionResult']
    list_filter = ['e_buyDate']
    search_fields = ['e_kit__k_name', 'e_type__m_name']

class EpiInline(admin.StackedInline):
    model=Epi
    extra = 1

class KitAdmin(admin.ModelAdmin):
    fieldsets = [
            (None, {'fields': ['k_name',
                               'k_owner']}),
            ]
    inlines = [EpiInline]


admin.site.register(Epi, EpiAdmin)
admin.site.register(Kit, KitAdmin)