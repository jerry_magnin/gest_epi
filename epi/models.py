from django.db import models

# Create your models here.

CATEGORIES = [
        ('textile', 'Textile'),
        ('metal', 'Métallique'),
        ('mixte', 'Mixte métal/textile')
        ]

RESULTS = [
        ('OK', 'OK'),
        ('AS', 'À surveiller'),
        ('AR', 'À réparer'),
        ('RE', 'Mise au rebut'),
        ]

class Type(models.Model):
    m_name = models.CharField(max_length=100, verbose_name="Nom de type")
    m_category = models.CharField(max_length=10,
                                  verbose_name="Catégorie",
                                  choices=CATEGORIES)
    m_parentType = models.ForeignKey('self', on_delete=models.CASCADE, verbose_name="Type parent", related_name="parent", null=True, blank=True)
   
    def __str__(self):
        return self.m_name

class Kit(models.Model):
    k_name = models.CharField(max_length=100,
                              verbose_name="Nom du kit")
    k_owner = models.CharField(max_length=100,
                               verbose_name="Utilisateur",
                               blank=True, null=True)
    
    def __str__(self):
        return self.k_name

class Brand(models.Model):
    b_name = models.CharField(max_length=100,
                              verbose_name="Nom")
    b_web = models.CharField(max_length=255,
                             verbose_name="Site web",
                             blank=True, null=True)
    
    def __str__(self):
        return self.b_name

class Epi(models.Model):
    e_brand = models.ForeignKey(Brand,
                                on_delete=models.CASCADE,
                                verbose_name="Marque")
    e_model = models.CharField(max_length=150,
                               verbose_name="Modèle")
    e_serial = models.CharField(max_length=100,
                                verbose_name="Numéro de série")
    e_type = models.ForeignKey(Type,
                               on_delete=models.CASCADE,
                               verbose_name="Type")
    e_kit = models.ForeignKey(Kit,
                              on_delete=models.CASCADE,
                              verbose_name="Kit d’appartenance")
    e_buyDate = models.DateField(verbose_name="Date d’achat")
    
    def __str__(self):
        nom = "{} {} {} - n°{} - Kit « {} »".format(self.e_type.m_name,
               self.e_brand.b_name,
               self.e_model,
               self.e_serial,
               self.e_kit.k_name)
        return nom

class Inspection(models.Model):
    e_date = models.DateField(verbose_name='Date d’inspection')
    e_epi = models.ForeignKey(Epi,
                              on_delete=models.CASCADE,
                              verbose_name='EPI')
    e_result = models.CharField(max_length=2,
                                verbose_name="Résultat",
                                choices=RESULTS)
    e_details = models.TextField(verbose_name="Détails", blank=True, null=True)
    
    def __str__(self):
        nom = "Inspection n°{} du {} | ".format(self.id, self.e_date)
        nom += "{} | Résultat : {}".format(self.e_epi, self.e_result)
        return nom
