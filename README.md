# Gest_EPI

Application web basée sur Django pour le suivi et le contrôle des EPI contre les chutes de hauteur (harnais, cordes, mousquetons…)

## Changelog
### Pre-alpha
Construction du cœur de l’application

## TODO

- [ ] Gérer la durée de vie des équipements
- [ ] Sur la page d’un kit, lien de la date d’inspection vers son résultat détaillé.
- [X] Sur la page d’un EPI (à créer…), lister les inspections.
- [ ] Basculer des templates DTL vers `jinja2`
